I. Create a file named credentials
 root path: /configs/aws/credentials

[terraform]
aws_access_key_id = YOUR_ACCESS_KEY_ID
aws_secret_access_key = YOUR_SECRET_ACCESS_KEY

1. Open a terminal or command prompt and navigate to the directory where your Terraform files are located.
2. Run the following command to initialize the Terraform configuration:
terraform init 
3. Next, run the command to validate the Terraform configuration:
terraform validate
4. Run the following command to see the execution plan and confirm the resources that Terraform will create:
terrafom plan
5. If the plan looks good, proceed to apply the changes by running the following command:
terraform apply
6. Cleaning Up:
terraform destroy

