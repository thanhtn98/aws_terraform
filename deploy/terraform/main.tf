
# module x_api {
#  source = "./modules/api"
#  vpc_id = module.x_vpc.vpc_id
#  subnet_ids = module.x_vpc.subnet_ids
#  ecs_cluster_id = module.x_ecs.ecs_cluster_id
#  eks_cluster_id = module.x_eks.eks_cluster_id
#  repository_id = "X/auth-api"
#  branch_name = "main"
#  domain = "api.x.com"
#  # ... any other variables that need to be set
#  depends_on = [
#  module.x_vpc,
#  module.x_ecs,
#  module.x_eks,
#  module.x_rds,
#  module.codecommit_infrastructure_source_repo
#  module.codebuild_terraform
#  module.codepipeline_kms

#  # ... any other modules that this module depends on
#  ]
# }
module webapp {
  source = "./modules/webapp"
  aws_region = "us-east-1"
  bucket_name = "MyCloudFront"
  domain_name = "api.x.com"
  aws_codebuild_project_name = "build-code"

depends_on = [
 module.x_vpc,
 module.x_ecs,
 module.x_eks,
 module.x_rds,
 # ... any other modules that this module depends on
 ]
}
